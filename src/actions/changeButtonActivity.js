export const changeButtonsActivity = (newActivity, key) => {
    return {
        type: 'LOCAL_ACTIVITY_BUTTON_CHANGE',
        newActivity: newActivity,
        key: key
    }
};