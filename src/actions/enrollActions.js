export const enroll = (client) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const firestore = getFirestore();
        firestore.collection('users').add({
            ...client,
            createdAt: new Date()            
        }).then(() => {
            dispatch({ type: 'CREATE_CLIENT', client: client });
        }).catch((err) => {
            dispatch({type: 'CREATE_CLIENT_ERROR', err})
        })
    }
};