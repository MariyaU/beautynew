import React from 'react';
import '../App.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import NavBar from './NavBar.jsx';
import Contacts from './contactsComponents/Contacts.jsx';
import SignUp from './SignUpComponents/SignUp.jsx';
import Service from './serviceComponents/Service.jsx';
import Home from './homeComponents/Home.jsx';
import ClientBase from '../components/clientBase/clientBase';


const App = () => {
  return (
    <BrowserRouter>
      <div>
        <NavBar />
        <Switch>
          <Route path='/home' component={Home} />
          <Route path='/service' component={Service} />
          <Route path='/signup' component={SignUp} />
          <Route path='/contacts' component={Contacts} />
          <Route path='/clients' component={ClientBase} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default App;