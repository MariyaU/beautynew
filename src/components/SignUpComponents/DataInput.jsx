import React from 'react';

const DataInput = props => {
    return (
        <div>
            <div>{props.name}</div>
            <input id = {props.id} onChange = {(event) => props.addPropsItem(props.name, event.target.value)} label={props.name} />
        </div>
    )
};

export default DataInput;