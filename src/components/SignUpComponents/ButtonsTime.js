import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeButtonsActivity } from '../../actions/changeButtonActivity';

class ButtonsTime extends Component {
    constructor(props) {
        super(props)
        this.state = { activity: this.props.activity, type: this.props.type }
    }
    handleClick = (e) => {
        // console.log('aaaaaaa', this.props.activity)
        let activity = {};
        for (let key in this.state.activity) {
            activity[key] = this.state.activity[key]
        }
        activity[e.target.value] = !activity[e.target.value];
        this.setState({
            activity
        })
        // console.log(this.state);
        console.log('отправляю на изменение', this.props.id, activity)
    
        this.props.change(activity, this.props.id);

    }
    render() {
        // console.log('state', this.props.activity)
        // console.log('state', this.state)

        return (
            <>
                {this.props.times.map((item, index) => {
                    return (
                        <button key = {index} id = {index} value = {item} onClick={this.handleClick} disabled={this.props.activity[item]} className='waves-effect waves-light btn-small'>{item}</button>
                    )
                })}
            </>
        )
    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        change: (inf, key) => dispatch(changeButtonsActivity(inf, key))
    }
}
export default connect(null, mapDispatchToProps)(ButtonsTime);