import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import ServiceList from './ServiceList';
import { changeActiveState } from '../../actions/stateActivityButtonsAction';

class Services extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        this.props.changeState(this.props.services)
        return (
            <ServiceList list={this.props.services} />
        )
    }

}

const mapStateToProps = (state) => {
    //  console.log('bigstate', state)
    const services = state.firestore.ordered.services;
    return {
        services: services
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        changeState: (newState) => dispatch(changeActiveState(newState))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'services' }
    ])
)
(Services);