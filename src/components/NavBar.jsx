import React from 'react';
import NavTab from './NavTab.jsx';
import { NavLink } from 'react-router-dom';
import home from '../img/home.png';


const NavBar = () => {
    return (
        <nav className="nav-wrapper grey darken-3">
            <div className="container">
                <NavLink to='/home'>
                    <img src={home} alt='картинка' />
                </NavLink>
                <div className='right'>
                    <NavTab to='/service' label='Услуги' />
                    <NavTab to='/signup' label='Записаться' />
                    <NavTab to='/contacts' label='Контакты' />
                    <NavTab to='/clients' label='Клиенты' />
                </div>
            </div>
        </nav>
    );
};

export default NavBar;