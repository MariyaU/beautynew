const enrollReducer = (state = {}, action) => {
    switch (action.type) {
        case 'CREATE_CLIENT':
            return state;
        case 'CREATE_CLIENT_ERROR':
            return state;
        default:
            return state;
    }
}

export default enrollReducer