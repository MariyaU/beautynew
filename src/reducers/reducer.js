import { combineReducers } from 'redux';
import buttonActivity from './buttonActivity.js';
import { firestoreReducer } from 'redux-firestore';

const reducer = combineReducers({
    buttonActivity,
    firestore: firestoreReducer
});

export default reducer;