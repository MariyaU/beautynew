
const buttonActivity = (state = [], action) => {
    switch (action.type) {
        case 'CHANGE_ACTIVE_BUTTON':
            console.log('change success');
            return state;
        case 'CHANGE_ACTIVE_BUTTON_ERROR':
            console.log('update error', action.err);
            return state;
        case 'CHANGE_ACTION_STATE':
            let stateBtn = action.newState;
            console.log(stateBtn)
            return {
                stateBtn
            }
        case 'LOCAL_ACTIVITY_BUTTON_CHANGE': {
            let stateBtn = [];
            let id = state.stateBtn[action.key].id;
            console.log('reducer', state.stateBtn);
            for (let i = 0; i < state.stateBtn.length; i++) {
                stateBtn.push(JSON.parse(JSON.stringify(state.stateBtn[i])));
            }
            stateBtn[action.key].time = action.newActivity;
            stateBtn[action.key].id = id;
            return {
                stateBtn
            }
        }
        default:
            return state;
    }
};

export default buttonActivity;