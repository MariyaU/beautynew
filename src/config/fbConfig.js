import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
var config = {
    apiKey: "AIzaSyBlA0X8rdE57OYROKHmpShEC9nl8H0mQGM",
    authDomain: "beautiful-ea0bd.firebaseapp.com",
    databaseURL: "https://beautiful-ea0bd.firebaseio.com",
    projectId: "beautiful-ea0bd",
    storageBucket: "beautiful-ea0bd.appspot.com",
    messagingSenderId: "83237859898",
    appId: "1:83237859898:web:8a41ac797d4ee5484cbb03",
    measurementId: "G-GBYKF63EWT"
  };
  // Initialize Firebase
  firebase.initializeApp(config);
  firebase.firestore().settings({ timestampsInSnapshots: true });

  export default firebase;